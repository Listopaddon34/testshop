﻿using System;
using DataAccess.Repositories.Session;
using BusinessLogic.LogicBusiness.Session;
using DataAccess.Models;

namespace Для_проверки
{
    internal class Program
    {
        static void Main(string[] args)
        {
            SessionLogic session = new SessionLogic();

            session.AddSession(2, 2, 67);
            session.UpdateSession(new SessionDto(1, 2, 1, 100));
            session.DeleteSession(4);
            session.GetSessions();
        }
    }
}
